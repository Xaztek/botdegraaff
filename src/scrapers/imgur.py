import random
import requests


class ImgurScraper:

    def __init__(self, client_id):
        self.client_id = client_id
        self.headers = {
            'Authorization': f'Client-ID {self.client_id}'
        }
    
    def convert_to_message(self, meme_tuple):
        """ convert the title and meme link to a discord message """
        return str('**{title}**\n{meme}'.format(
            title=meme_tuple[0],
            meme=meme_tuple[1])
        )

    def get_gallery(self, searchq, sortq='top'):
        """ Get the json of all images in an Imgur gallery """
        url = f'https://api.imgur.com/3/gallery/search/{sortq}/all/0?q={searchq}'
        r = requests.get(url=url, headers=self.headers)
        r.raise_for_status()
        return r.json()

    def get_random_meme(self, jdata):
        """ return a random meme from the search query """
        # TODO: Store the memes in a database
        found_memes = []
        if jdata['data']:
            for meme in jdata['data']:
                found_memes.append((meme['title'], meme['link']))
            return self.convert_to_message(random.choice(found_memes))
        else:
            return 'No memes were found :('

    def run(self, searchq):
        result = self.get_gallery(searchq)
        return self.get_random_meme(result)