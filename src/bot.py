import random

from discord.ext import commands
from simple_settings import settings

from scrapers.imgur import ImgurScraper

bot = commands.Bot(command_prefix='!')


@bot.event
async def on_ready():
    """ function that is run when the Bot connects to discord """
    print(f'{bot.user.name} has connected to Discord!')

@bot.command(name='meme')
async def _meme(ctx, *args):
    """ 
    function that will get a meme based on a search term
    command: !meme <user-input>
    """
    imgurq = ' '.join(args)
    imgur = ImgurScraper(imgur_client_id)
    meme = imgur.run(imgurq)
    await ctx.send(meme)

@bot.command(name='hello')
async def _hello(ctx):
    """ reply to !hello """
    replies = ['Hey', "What's up?", 'hoe ist?']
    await ctx.send(random.choice(replies))

@bot.command(name='foo')
async def _foo(ctx, *arg):
    await ctx.send('{} arguments: {}'.format(len(args), ', '.join(args)))


if __name__ == '__main__':
    settings = settings.as_dict()
    imgur_client_id = settings['IMGUR_CLIENT_ID']
    token = settings['TOKEN']
    bot.run(token)