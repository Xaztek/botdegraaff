###########################################################################################
## This is a temple file, you will need to arrange your own keys and rename it as prd.py 
## To run the bot execute python bot.py --settings=settings.prd                          
###########################################################################################


# Discord client
GUILD = 'xxx'
TOKEN = 'xxx'
EMAIL = 'xxx'

# Reddit
REDDIT_USERNAME = 'xxx'
REDDIT_PASSWORD = 'xxx'
REDDIT_CLIENT_ID = 'xxx'
REDDIT_CLIENT_SECRET = 'xxx'

# Imgur
IMGUR_USERNAME = 'xxx'
IMGUR_PASSWORD = 'xxx'
IMGUR_CLIENT_ID = 'xxx'
IMGUR_CLIENT_SECRET = 'xxx'