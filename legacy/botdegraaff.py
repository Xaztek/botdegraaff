#!/usr/bin/python3.5

import asyncio
import random
import os
import time
import re
import discord
import requests
import configparser
import json

client = discord.Client()

# These store all the data
author_bad_scores = {}
insult_words = {}


if not os.path.isfile('memes.json'):
    open('memes.json', 'a+').close()


json_memes = json.load(open('memes.json'))
memes = json_memes


def get_key(key):
    config = configparser.ConfigParser()
    config.read('config.ini')
    key = config.get('BOT', key)
    return key


def get_meme_count(memes):
    meme_count = re.findall('imgur', str(memes))
    meme_count = len(meme_count)
    print('meme count: {}'.format(meme_count))
    return meme_count


@client.event
async def on_ready():
    """ we are successfully connected to discord """
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    get_meme_count(memes)
    print('------')


@client.event
async def on_message(message):
    """ checks all messages on server """

    """ gives me bot admin powers 
    removed id since it's not longer used
    """
    if message.author.id == 'xxx':
        message_author = 'botdegraaf'

    if message.content.startswith('agreed'):
        if message.author == 'botdegraaff':
            await client.send_message(message.channel, ':+1:')
        else:
            await client.send_message(message.channel, ':rage:')


    message.content = message.content.lower()

    if message.content.startswith('!memecount'):
        await client.send_message(message.channel, 'Bot de Graaff has {} memes'.format(get_meme_count(memes)))


    if message.content.startswith('!howto'):
        await client.send_message(message.channel, how_to())


    #  loops though the memes list to look if any activation words are triggered
    for name, _word in memes.items():
        if _word['activation_word'] in message.content:
            if message.author == client.user:
                return None
            else:
                await client.send_message(message.channel, show_meme(memes[name]))
                time.sleep(1)


    if '9gag' in message.content:
        await client.send_message(message.channel, 'Bot de Graaf disapproves! :rage:')


    if 'chicken dinner' in message.content:
        await client.send_message(message.channel, 'http://i.imgur.com/M3Sor.jpg')


    if message.content.startswith('!reportbug'):
        await client.send_message(message.channel, 'Received bug report: {}'.format(message.content.split('!reportbug')[1]))
        time.sleep(1)
        await client.send_message(message.channel, 'Removed bug report: {}.\nBot de Graaf doesn\'t have bugs, only features.'.format(message.content.split('!reportbug')[1]))


    # show all avaiable meme streams
    if '!memestream' in message.content:
        if message.author == client.user:
            return None
        else:
            await client.send_message(message.channel, get_meme_stream())
            return

    # remove unwanted meme page
    if message.content.startswith('!remove'):
        message.content = message.content.split('!remove')[1].strip()
        write_to_json(memes)
        await client.send_message(message.channel, remove_meme_page(message, message.content))

    # add meme page
    if message.content.startswith('!addmemes'):
        got_memes_bool_message = add_new_meme_source(message, memes)
        if message.author == client.user:
            return None
        else:
            if got_memes_bool_message[0] is True:
                await client.send_message(message.channel, got_memes_bool_message[1])
            else:
                await client.send_message(message.channel, 'Failed to add '+ got_memes_bool_message[1])
        write_to_json(memes)
        get_meme_count(memes)

	#FIXME: this shit is broken AF
    # get new memes for each page
    if message.content.startswith('!refreshmemes'):
        if message.author == client.user or 'botdegraaf':
            await client.send_message(message.channel, 'getting fresh memes')
            get_new_memes(memes)
            await client.send_message(message.channel, 'refreshed the meme stream (count: {}'.format(get_meme_count(memes)))
        else:
            await client.send_message(message.channel, 'Only I can do this.')

    if 'jim' in message.content:
        if message.author == client.user:
            return None
        else:
            time.sleep(3)
            await client.send_message(message.channel, 'Jim heeft gelijk!')

    if message.content.startswith('roll '):
        die = int(message.content.split()[1])
        die_roll = roll_die(die)
        if die_roll is False:
            await client.send_message(message.channel, 'Invalid die')
            return None
        await client.send_message(message.channel, '{name} rolled a {roll} on a D{die}'.format(name=str(message.author).split('#')[0], roll=roll_die(die), die=die))

def how_to():
    commands = [
        '*show meme stream*: displays all trigger words',
        '*!addmemes {{imgur url}} {{trigger word}}*:  add a imgur page to the meme stream',
        '*!reportbug {{bug}}*:  report a bug to botdegraaf',
        ]
    message = '\n\n'.join(commands)
    return message


def correct_word(author, word):
    """ warns user of their bad language """
    author_bad_scores[author] = author_bad_scores.setdefault(author, 0) + 1
    return 'Woah <@{}> [Biggot Points: {}] please refrain from using such a bigoted term as **{}** and use the correct term **{}**.'.format(author.id, author_bad_scores[author], word, correct_words[word])


def shuffle_memes(meme_key):
    """ shuffles the memes on meme key """
    return random.shuffle(meme_key['memes'])


def show_meme(meme_key):
    #TODO: change this so it has the memes list
    """ shows meme and returns the first meme then sends it to the back of the list """
    meme = meme_key['memes'][0]
    meme_key['memes'].append(meme)
    meme_key['memes'].remove(meme)
    return meme


def get_meme_stream():
    stream = []
    for name, meme in memes.items():
        stream.append(meme['activation_word'])
    stream = '\n'.join(stream)
    return stream


def remove_meme_page(message, meme_page):
    """ removes a meme page from the meme stream """
    if message.author.id == 'xxx':
        for key, value in memes.items():
            if meme_page in value['activation_word']:
                del memes[key]
                return 'successfully removed {}.'.format(meme_page)
    else:
        return 'Only I can do this'
    write_to_json(memes)



def vote_meme_page(vote):
    if type(vote) != bool:
        pass


def add_new_meme_source(message, memes_dict):
    """ adds a new imagur source of fresh memes """
    #TODO: rewrite this crap
    message_list = message.content.split()
    message_author = message.author.id
    # I got infite adds :D
    if message_author == 'xxx':
        message_author = 'botdegraaf'
    url = message_list[1]
    if not url.startswith('https://imgur.com/'):
        print('Meme source must be from an imgur page. (atleast for now)')
        return [False, 'Meme source must be from an imgur source for the time being.']
    if '/gallery' in url:
        return [False, 'Imgur API is gay and sucks for getting galleries. Try /t/ags or some other page']
    name = url.split('https://imgur.com/')[1]
    if name in memes_dict:
        print('aready exists')
        return [False, 'url already exists.']
    if len(message_list) > 7:
        print('Message trigger must be less than 5 words')
        return [False, 'Message trigger must be less than 5 words']
    act_word = ' '.join(message_list[2:])
    for key, value in memes_dict.items():
        if act_word in value['activation_word']:
            if message.author == client.user:
                return None
            else:
                print (key, 'already exists')
                return [False, '{} already exists.'.format(key)]
        if message_author in value['added_by'] and not 'botdegraaf' in value['added_by']:
            print('you already added a source')
            return [False, '<@{}> you already added a meme source'.format(message.author.id)]
    memes_dict.update({
        name : {'imgur_url': url, 'activation_word': act_word, 'added_by': message_author, 'rep':1, 'memes': []}
    })
    get_imgur_memes(memes_dict[name])
    if memes_dict[name]['memes'] == []:
        del memes_dict[name]
        print('deleting entry ',name)
        return [False, 'no memes found']
    shuffle_memes(memes_dict[name])
    return [True, 'Successfully added {} to the meme stream. :ok_hand:\nNew meme count: {}'.format(memes_dict[name]['activation_word'], get_meme_count(memes))]


def get_imgur_memes(imgur_page):
    """ gets imgur memes through api """
    url = imgur_page['imgur_url']
    if not url.endswith('/'):
        url = url + '/'
    r_url = 'https://api.imgur.com/3/gallery/' + url.split('com/')[1]
    r = requests.get(r_url, headers=imgur_headers)
    if r.status_code == 200:
        content = str(r.content)
        items = imgur_meme_re.findall(content)
        for item in items:
            if '/t/' in url:
                url = 'https://imgur.com/gallery/'
            item = url+item
            #don't add duplicates
            if item not in imgur_page['memes']:
                imgur_page['memes'].append(item)


def get_new_memes(memes_dict):
    """ runs at start to collect memes """
    for meme_page in memes_dict:
        time.sleep(1)
        get_imgur_memes(memes_dict[meme_page])
        shuffle_memes(memes_dict[meme_page])
    write_to_json(memes_dict)



def write_to_json(memes_dict):
    with open('memes.json','w') as json_file:
        json.dump(memes_dict, json_file, indent=4, sort_keys=True)
    #print('about to close json file')
    json_file.close()
    #print('closed json file')
    json_memes = json.load(open('memes.json'))
    #print('loaded new json file')
    memes = json_memes
    #print('returning memes')
    return memes

def roll_die(die):
    dice = (2, 3, 4, 6, 8, 10, 12, 20, 100)
    if die in dice:
        return random.randint(0, die)
    return False 


def main():
    """ starts the bot """
    get_new_memes(memes)
    discord_key = get_key('discord')
    client.run(discord_key)


imgur_client_id = get_key('imgur')
imgur_headers = {'Authorization': 'Client-ID '+imgur_client_id}
imgur_meme_re = re.compile('\},\{"id":"([^"]+)')


if __name__ == '__main__':
    main()
